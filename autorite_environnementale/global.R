library(shiny)
library(shinydashboard)
library(shinythemes)
library(config)
library(DBI)
library(RPostgreSQL)
library(tidyr)
library(dplyr)
library(plyr)
library(dbplyr)
library(DT)
library(lubridate)
library(highcharter)
library(viridisLite)
library(xtable)
library(shinycssloaders)
library(RColorBrewer)
library(glue)

rm(list = ls())
options(shiny.sanitize.errors = FALSE)

# Paramétrage de la connexion au SGBD
dw <- config::get("datawarehouse")
con <-
  dbConnect(
    drv = dw$driver,
    dbname = dw$database,
    host = dw$server,
    port = dw$port,
    user = dw$uid,
    password = dw$pwd
  )
on.exit(dbDisconnect(con))

# Chargement de la table avis_ae.export_garance
bilan_activite <-
  dbGetQuery(con,
    "SELECT * FROM avis_ae.export_garance WHERE type_consultation IN ('AEPP', 'AEPP-CC', 'AEPR', 'AEPR-CC') ORDER BY numero")
departements <-
  dbGetQuery(con,
             "SELECT insee_dep FROM avis_ae.n_departement_exp_r52 ORDER BY insee_dep")

if (!empty(bilan_activite)) {
  # Encodage (pour les environnements Windows)
  Encoding(bilan_activite$intitule_operation) <- "UTF-8"
  Encoding(bilan_activite$communes) <- "UTF-8"
  Encoding(bilan_activite$petitionnaire) <- "UTF-8"
  Encoding(bilan_activite$domaine) <- "UTF-8"
  Encoding(bilan_activite$categorie) <- "UTF-8"
  Encoding(bilan_activite$statut) <- "UTF-8"
  Encoding(bilan_activite$procedure) <- "UTF-8"
  # Conversion des dates au format dd/mm/YYYY
  bilan_activite$date_reception <- format(bilan_activite$date_reception, "%d/%m/%Y")
  bilan_activite$date_limite <- format(bilan_activite$date_limite, "%d/%m/%Y")
  bilan_activite$date_tacite <- format(bilan_activite$date_tacite, "%d/%m/%Y")
  bilan_activite$date_final <- format(bilan_activite$date_final, "%d/%m/%Y")
  bilan_activite$date_recours <- format(bilan_activite$date_recours, "%d/%m/%Y")
  bilan_activite$date_maintien <- format(bilan_activite$date_maintien, "%d/%m/%Y")
  bilan_activite$date_nouvelle_decision <- format(bilan_activite$date_nouvelle_decision, "%d/%m/%Y")
  # Conversion des champs à filtrer en facteurs
  bilan_activite$numero <- as.character(bilan_activite$numero)
  bilan_activite$departement <- as.factor(bilan_activite$departement)
  bilan_activite$domaine <- as.factor(bilan_activite$domaine)
  bilan_activite$categorie <- as.factor(bilan_activite$categorie)
  bilan_activite$document_urbanisme <- as.factor(bilan_activite$document_urbanisme)
  bilan_activite$soumission_ee <- as.factor(bilan_activite$soumission_ee)
  bilan_activite$procedure <- as.factor(bilan_activite$procedure)
  bilan_activite$trimestre <- as.factor(bilan_activite$trimestre)
  bilan_activite$type_consultation <- as.factor(bilan_activite$type_consultation)
  departements$insee_dep <- as.factor(departements$insee_dep)
}